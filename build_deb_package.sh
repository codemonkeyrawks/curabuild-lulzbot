#!/bin/bash
mkdir -p build
cd build
cmake -DCURA_TAG_OR_BRANCH=master -DURANIUM_TAG_OR_BRANCH=master -DCURA_ENGINE_TAG_OR_BRANCH=master -DARCUS_TAG_OR_BRANCH=master -DSAVITAR_TAG_OR_BRANCH=master ../
echo "You should be able to monitor progress via \"tail -f `pwd`/cura-lulzbot_build.log\" in another terminal"
echo "After build is done you can run CuraLE with \"cd `pwd`/dist; LD_LIBRARY_PATH=\`pwd\` ./cura-lulzbot\""
CC=gcc CXX=g++ F90=gfortran PYTHONPATH=`pwd`/inst/lib/python3.5/site-packages PATH=`pwd`/inst/bin:${PATH} LD_LIBRARY_PATH=`pwd`/inst/lib make package &> cura-lulzbot_build.log
cd ../
